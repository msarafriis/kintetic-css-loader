# Kinetic CSS Loader

A CSS-only loader. Nice.

Could be useful in a UI library.

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).
